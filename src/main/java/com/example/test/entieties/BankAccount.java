package com.example.test.entieties;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "bank_account")
public class BankAccount {

    // === Fields ===
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int Id;

    private long volume;

    @Enumerated(EnumType.STRING)
    Currency currency;

    // relacje ponizej
    @ManyToOne
    User user;

    @OneToMany(mappedBy = "source_account")
    private List<Transaction> source_transactions;

    @OneToMany(mappedBy = "target_account")
    private List<Transaction> target_transactions;

// === getters and setters ===
    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return this.user;
    }

    public long getVolume() {
        return volume;
    }

    public void setVolume(long volume) {
        this.volume = volume;
    }

    public List<Transaction> getSource_transactions() {
        return source_transactions;
    }

    public void setSource_transactions(List<Transaction> source_transactions) {
        this.source_transactions = source_transactions;
    }

    public List<Transaction> getTarget_transactions() {
        return target_transactions;
    }

    public void setTarget_transactions(List<Transaction> target_transactions) {
        this.target_transactions = target_transactions;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public int getId() {
        return Id;
    }

    // ===  custom methods ===

}
