package com.example.test.entieties;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "user")
public class User {
    // === Fields ===

    @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;

    private String password;
//zmienione cascadeType z ALL na Merge
    @OneToMany(mappedBy = "user", cascade = CascadeType.MERGE)
    private List<Transaction> transactions;

    @OneToMany(mappedBy = "user", cascade = CascadeType.MERGE)
    private List<BankAccount> accounts;

    // === getters and setters ===

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public List<BankAccount> getAccounts() {
        return accounts;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    // === custom methods ===

    public void addAccountToUser(BankAccount bankAccount) {
        accounts.add(bankAccount);
        bankAccount.setUser(this);
    }

    public void addTransactionToUser(Transaction transaction) {
        transactions.add(transaction);
        transaction.setUser(this);
    }


}
