package com.example.test.entieties;


import javax.persistence.*;

@Entity
@Table(name = "transaction")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int Id;

    private  long cashAmount;

    @ManyToOne
    private User user;

// @ManyToOne moim zdaniem a nie One to One
    @ManyToOne//(mappedBy = "sourceTransactions")
    BankAccount source_account;

    @ManyToOne
    BankAccount target_account;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }



    public Transaction() {
    }

    public BankAccount getSource_account() {
        return source_account;
    }

    public void setSource_account(BankAccount source_account) {
        this.source_account = source_account;
    }



    public long getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(long cashAmount) {
        this.cashAmount = cashAmount;
    }

    public BankAccount getTarget_account() {
        return target_account;
    }

    public void setTarget_account(BankAccount target_account) {
        this.target_account = target_account;
    }
}
