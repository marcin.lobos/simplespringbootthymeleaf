package com.example.test.controlers;


import com.example.test.entieties.BankAccount;
import com.example.test.entieties.Currency;
import com.example.test.entieties.User;
import com.example.test.repositories.BankAccountRepository;
import com.example.test.repositories.UserRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class BankAccountControler {


    BankAccountRepository bankAccountRepository;
    UserRepository userRepository;

    public BankAccountControler(BankAccountRepository bankAccountRepository, UserRepository userRepository) {
        this.bankAccountRepository = bankAccountRepository;
        this.userRepository = userRepository;
    }

    @GetMapping("/showAddAccountTempl")
    public String showAddAccountTemplate(Model model, @RequestParam("id") Integer id) {
        model.addAttribute("currentUser", userRepository.findById(id).orElse(null));
        model.addAttribute("currencys", Currency.values());
        model.addAttribute("bankAccount", new BankAccount());
        return "add-user-account";
    }


    @PostMapping("/addAccountToUserAndDB")
    public String addAccountToUser(Model model, BankAccount bankAccount, @RequestParam("idUser") Integer idUser) {

        User user = userRepository.findById(idUser).orElse(null);
        user.addAccountToUser(bankAccount);
        bankAccountRepository.save(bankAccount);
        return "redirect:/showAddAccountTempl?id=" + idUser;

    }
}
