package com.example.test.controlers;

import com.example.test.entieties.User;
import com.example.test.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class UserControler {

    private UserRepository userRepository;

    //    === przekaz obiektu reposityrory poprzez konstruktor ===
    @Autowired
    public UserControler(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/showUserList")
    public String showUserList(Model model) {

        model.addAttribute("users", userRepository.findAll());
        return "user-list01";
    }



    @GetMapping("/signup")
    public String showFormToAddUser(User user) {
        return "add-user2";
    }

    @PostMapping("/adduser")
    public String addUserToDB(@Valid User user, Model model, BindingResult result) {
        if (result.hasErrors()) {
            return "add-user2";
        }
        userRepository.save(user);
        model.addAttribute("users", userRepository.findAll());
        return "user-list01";
    }

    @GetMapping("/deluser") // lepiej przez parametr nic przez patch variable bo robi gupoty
    public String deleteUserById(Model model, @RequestParam("id") Integer id) {

        userRepository.deleteById(id);
        model.addAttribute("users", userRepository.findAll());
        return "index";
    }


    @GetMapping("/editUser")
    public String updateUser(@Valid User user, Model model, BindingResult result, @RequestParam("id") Integer id) {

        User user1= userRepository.findById(id).orElseThrow( () -> (new  IllegalArgumentException("niewlasciwy id usera")) );
        model.addAttribute("user", user1);
        return "update-user2";
    }


    @PostMapping("/updateUser")
    public String editUserInDB(@Valid User user, Model model, BindingResult result) {
        if (result.hasErrors()) {
            return "update-user2";
        }
        userRepository.save(user);
        model.addAttribute("users", userRepository.findAll());
        return "user-list01";
    }


    @GetMapping("/showUserPanel")
    public String showUserPanel(Model model) {
      User loggedUser = userRepository.findById(1).orElse(null);
        model.addAttribute("loggedUser", loggedUser);
        return "user-page";
    }

}
