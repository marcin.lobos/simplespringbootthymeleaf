package com.example.test.controlers;

import com.example.test.entieties.Transaction;
import com.example.test.entieties.User;
import com.example.test.repositories.BankAccountRepository;
import com.example.test.repositories.TransactionRepository;
import com.example.test.repositories.UserRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class TransactionControler {


    // Dodane bankRepository ------ nie wiemy czy być musi!!!!!!!!!!!!!
    TransactionRepository transactionRepository;
    UserRepository userRepository;
    BankAccountRepository bankAccountRepository;

    public TransactionControler(TransactionRepository transactionRepository, UserRepository userRepository, BankAccountRepository bankAccountRepository) {
        this.transactionRepository = transactionRepository;
        this.userRepository = userRepository;
        this.bankAccountRepository = bankAccountRepository;
    }

    @GetMapping("/showAddTransaction")
    public String showAddTransaction(Model model, @RequestParam("id") Integer id) {
        model.addAttribute("currentUser", userRepository.findById(id).orElse(null));
        model.addAttribute("transaction", new Transaction());

        return "add-transaction";
    }

    @PostMapping("/addTransactionToUser")
    public String addTransactionToUser(Model model, Transaction transaction, @RequestParam("id") Integer id) {
        User user = userRepository.findById(id).orElse(null);
        user.addTransactionToUser(transaction);
        transactionRepository.save(transaction);

        return "redirect:/showAddTransaction?id=" + id;
    }


    @GetMapping("/showTransactionList")
    public String showTransactionList(Model model) {
        model.addAttribute("transactions", transactionRepository.findAll());

        return "transaction-list";
    }


    @GetMapping("/counsumeTransaction")
    public String counsumeTransaction(Model model, @RequestParam("idTr") Integer idTr, @RequestParam("idU") Integer idU) {

        User user = userRepository.findById(idU).orElse(null);
        Transaction transaction = transactionRepository.findById(idTr).orElse(null);

        long transactionAmount = transaction.getCashAmount();
        long oldAccountVolume = user.getAccounts().get(0).getVolume();
        long newAccountVolume = oldAccountVolume - transactionAmount;

        user.getAccounts().get(0).setVolume(newAccountVolume);


        long oldAccountVolume2 = user.getAccounts().get(1).getVolume();
        long newAccountVolume2 = oldAccountVolume2 + transactionAmount;

        user.getAccounts().get(1).setVolume(newAccountVolume2);

        userRepository.save(user);

        transactionRepository.delete(transaction);
        model.addAttribute("transactions", transactionRepository.findAll());

        return "redirect:/showTransactionList";
    }

    @GetMapping("/delTransaction")
    public String delTransaction(Model model, @RequestParam ("idTr") Integer idTr) {


        transactionRepository.deleteById(idTr);
        model.addAttribute("transactions", transactionRepository.findAll());

        return "redirect:/showUserPanel";
    }
}
