package com.example.test.services;

import com.example.test.entieties.BankAccount;
import com.example.test.entieties.User;
import com.example.test.repositories.BankAccountRepository;
import com.example.test.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    BankAccountRepository bankAccountRepository;

    public void createUser(User user, BankAccount usdBankAccound, BankAccount plnBankAccount){
//
        int id = user.getId();
        userRepository.save(user);

        usdBankAccound.setUser(userRepository.findById(id).get());

        userRepository.save(user);
        bankAccountRepository.save(usdBankAccound);


        plnBankAccount.setUser(userRepository.findById(id).get());
        bankAccountRepository.save(plnBankAccount);
    }


    public User getUser( int id){
        return userRepository.findById(id).orElse(null);

    }
}
