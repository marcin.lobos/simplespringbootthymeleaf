package com.example.test.services;


import com.example.test.entieties.BankAccount;
import com.example.test.entieties.User;
import com.example.test.repositories.BankAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BankAccountService {

    @Autowired
    BankAccountRepository bankAccountRepository;

    public void topUpAccount (BankAccount bankAccount, User user, long volume){
        bankAccount.setUser(user);
//        bankAccount.setVolume(volume);

        bankAccountRepository.save(bankAccount);
    }


}
