package com.example.test.repositories;

import com.example.test.entieties.Transaction;
import com.example.test.entieties.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserTransactionRepositoryIT {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Transactional
    @Test
    public void testCreate() {

        // new user
        User user = new User();
        user.setName("Adam");

        user = userRepository.save(user);
        // new Transaction

        Transaction transaction = new Transaction();

        transaction.setUser(user);

        transaction =  transactionRepository.save(transaction);

        User u = transaction.getUser();

        // Asercje
        assertEquals("Adam",u.getName());


    }

}