//package com.example.test.repositories;
//
//import com.example.test.entieties.BankAccount;
//import com.example.test.entieties.Transaction;
//import com.example.test.entieties.User;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
//import static org.junit.Assert.assertEquals;
//
//@SpringBootTest
//@RunWith(SpringRunner.class)
//public class UserTransactionAcountsRepositoryIT {
//
//    @Autowired
//    private UserRepository userRepository;
//
//    @Autowired
//    private TransactionRepository transactionRepository;
//
//    @Autowired
//    private BankAccountRepository bankAccountRepository;
//
//    @Transactional
//    @Test
//    public void testConnectionTransToAccount() {
//
//        // create users
//        User user1 = new User();
//        user1.setName("Adam");
//
//        User user2 = new User();
//        user2.setName("Tomek");
//
//        // create acounts
//
//        BankAccount sourceAccount = new BankAccount();
//        sourceAccount.setVolume(100);
//
//        BankAccount targetAccount = new BankAccount();
//        targetAccount.setVolume(100);
//
//        // create transaction
//
//        Transaction transaction = new Transaction();
//        transaction.setCashAmount(5);
//
//        // ==========   change date beetwen objects =============
//
//        transaction.setSource_account(sourceAccount);
////        transaction.setTarget_account(targetAccount);
//
//        user1.getAccounts().add(sourceAccount);
//        user2.getAccounts().add(targetAccount);
//
//        // === save to DB ===
//        userRepository.save(user1);
//        userRepository.save(user2);
//
//        bankAccountRepository.save(sourceAccount);
//        bankAccountRepository.save(targetAccount);
//
//        transactionRepository.save(transaction);
//
//        // === make transaction ===
//        List<Transaction> transactionList = (List<Transaction>) transactionRepository.findAll();
//        Transaction transaction1 = transactionList.get(0);
//        // make operaction on transaction
//        long moneyToChange = transaction1.getCashAmount();
//        long moneyOnStartAccount = transaction1.getSource_account().getVolume();
//        long moneyOnTargetAccount = transaction1.getTarget_account().getVolume();
//
//        transaction1.getSource_account().setVolume(moneyOnStartAccount - moneyToChange);
//        transaction1.getTarget_account().setVolume(moneyOnTargetAccount + moneyToChange);
//
//
//        Transaction transaction2 = transactionRepository.save(transaction1);
//
//        long moneyOnSourceAfter = transaction2.getSource_account().getVolume();
//
//
//
//
//        // Asercje
//        assertEquals(95, moneyOnSourceAfter);
//
//
//    }
//
//}