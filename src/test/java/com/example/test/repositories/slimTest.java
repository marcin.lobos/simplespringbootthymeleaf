package com.example.test.repositories;

import com.example.test.entieties.BankAccount;
import com.example.test.entieties.Transaction;
import com.example.test.entieties.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;

//@Transactional
@SpringBootTest
@RunWith(SpringRunner.class)
public class slimTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Transactional
    @Test
    public void slimTest() {

        // create users
        User user1 = new User();
        user1.setName("Adam");
        user1 = userRepository.save(user1);

//        User user2 = new User();
//        user2.setName("Tomek");
//        user2 = userRepository.save(user2);

        // create acounts

        BankAccount sourceAccount = new BankAccount();
//        sourceAccount.setVolume(100);
        sourceAccount.setUser(user1);
        bankAccountRepository.save(sourceAccount);

//        BankAccount targetAccount = new BankAccount();
//        targetAccount.setVolume(100);
//        targetAccount.setUser(user2);
//        bankAccountRepository.save(targetAccount);

        // create transaction

        Transaction transaction = new Transaction();
        transaction.setCashAmount(5);

        transaction = transactionRepository.save(transaction);
        // ==========   change date beetwen objects =============

        transaction.setSource_account(sourceAccount);

//        user1.getAccounts().add(sourceAccount);
//        user2.getAccounts().add(targetAccount);

        // === save to DB ===
//        userRepository.save(user1);
//        userRepository.save(user2);
//
//        bankAccountRepository.save(sourceAccount);
//        bankAccountRepository.save(targetAccount);

        Transaction transaction1 = transactionRepository.save(transaction);

        // === make transaction ===


//        long moneyOnSourceAfter = transaction1.getSource_account().getVolume();
//        long moneyOnSourceAfter = sourceAccount.getVolume();

        // Asercje
//        assertEquals(100, moneyOnSourceAfter);


//        List<Transaction> transactionList = transaction1.getSource_account().getSource_transactions();
        List<Transaction> transactionList1 = new ArrayList<>();
        transactionList1.add(transaction);
        BankAccount bbb = transaction1.getSource_account();
        transactionList1=  bbb.getSource_transactions();

        if(transactionList1 == null)
            System.out.println("============   List is null ==============================================!");
        else
            System.out.println("list is not Empty. =========================");
        bbb.getTarget_transactions();
// lista tranzakcji jest pusta, problem z mapowaniem relacji ??

        assertFalse(false);
    }

}