package com.example.test.repositories;

import com.example.test.entieties.BankAccount;
import com.example.test.entieties.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class BankAccountUserRepositoryIT {

    @Autowired
    BankAccountRepository bankAccountRepository;

    @Autowired
    UserRepository userRepository;

    @Transactional
    @Test
    public void getUserAccounts() {

        // new user
        User user = new User();
        user.setName("Adam");

        user = userRepository.save(user);

        BankAccount bankAccount = new BankAccount();
        bankAccount.setUser(user);

        bankAccount = bankAccountRepository.save(bankAccount);

        User u = bankAccount.getUser();
        assertEquals("Adam", u.getName());



    }

}